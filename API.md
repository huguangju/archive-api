API根路径: **127.0.0.1:10000**

## 获取列表

`GET /archive`

+ 示例：
  - `/archive?title=abc`
  - `/archive?pageSize=20&pageNumber=3` (每页20条，第3页)


+ 参数
	- `order = asc`   (固定不变,按照添加顺序从最开始添加到最后添加的顺序)
  - `title = `      (题名模糊搜索)

+ 排序
	- `pageNumber = 1`   (当前页, 默认为1)
	- `pageSize = 15` (页大小，默认一页10条)
	- `type = Qing`  (类型Qing代表清朝  Min代表民国)

+ 返回结构
	- 正确:
    ```js
			{
				total: 100, // 总条数
				rows: [     // 长度为pageSize的数据量
					object,   // 参考编辑的时候提交的数据单元结构，一模一样
					object,
				]
			}
    ``` 

	- 错误:
    ```js
    {
      total: 0, 
      rows: []
    }
    ```

## 新增

`POST /archive`

> 以 `x-www-form-urlencode` 方式传入新增数据

新增表单数据结构如下

```js
{
  "type": "Qing", //档案朝代类别
  "fullName": "张三96", //全宗名称
  "year": 1886, //年度
  "keepDate": 1507970502387, //保管期限
  "boxNo": "123421", //盒号
  "unitNo": "234132", //件号
  "title": "题名96", //题名
  "liablePerson": "责任人", //责任者
  "time": 1507970502388, //时间
  "pageCount": 219, //页数
  "secretLevel": 99, //密级
  "remark": "备注96", //备注
  "fileType": "清朝档案", //档案类别
  "folderName":"0d5eb32082-eee86-6a885-b653c-e3799edcb93be5a" // 图片文件夹路径
}
```

+ 返回结构使用
  - 正确:
    ```js
    {
      id: 1, // 数据库为新增数据生成的id
      state：true,
      msg:"添加成功",
    }
    ```

  - 错误
    ```js
    {
      state：false,
      msg:"添加失败",
    }
    ```

## 获取单条数据

`GET /archive/:id`

> 通过id获取对应数据，数据结构与增/改一致

+ 返回结构使用
  - 正确:
    ```js
    {
      "id": 1,
      "type": "Qing", // 档案朝代类别
      "fullName": "张三96", //全宗名称
      "year": 1886, //年度
      "keepDate": 1507970502387, //保管期限
      "boxNo": "123421", //盒号
      "unitNo": "234132", //件号
      "title": "题名96", //题名
      "liablePerson": "责任人", //责任者
      "time": 1507970502388, //时间
      "pageCount": 219, //页数
      "secretLevel": 99, //密级
      "remark": "备注96", //备注
      "fileType": "清朝档案", //档案类别
      "folderName":"0d5eb32082-eee86-6a885-b653c-e3799edcb93be5a" // 图片文件夹路径
    }
    ```

  - 错误
    ```js
    {}
    ```

## 修改

`PUT /archive/:id`

> id: 由数据库自动生成的主键

例：/archive/1, 同时以 `x-www-form-urlencode` 方式传入与新增时相同结构的数据

```js
{
  "type": "Qing", // 档案朝代类别
  "fullName": "张三96", //全宗名称
  "year": 1886, //年度
  "keepDate": 1507970502387, //保管期限
  "boxNo": "123421", //盒号
  "unitNo": "234132", //件号
  "title": "题名96", //题名
  "liablePerson": "责任人", //责任者
  "time": 1507970502388, //时间
  "pageCount": 219, //页数
  "secretLevel": 99, //密级
  "remark": "备注96", //备注
  "fileType": "清朝档案", //档案类别
  "folderName":"0d5eb32082-eee86-6a885-b653c-e3799edcb93be5a" // 图片文件夹路径
}
```

+ 返回结构使用
  - 正确:
    ```js
    {
      state：true,
      msg:"修改成功",
    }
    ```

  - 错误
    ```js
    {
      state：false,
      msg:"修改失败",
    }
    ```

## 删除

`DELETE /archive/:ids`

> ids 为 1,2,3 等以英文逗号间隔的id列表，可删除1条或同时删除多条数据，删除成功会通过 changes 返回此次删除数据的条数。
> 注：如果ids（1,2,100）中有不存在的id 100，则该条会被忽略，1，2仍会被正确删除，changes为2

+ 返回结构使用
  - 正确:
    ```js
    {
      changes: 3,
      state：true,
      msg:"删除成功",
    }
    ```

  - 错误
    ```js
    {
      state：false,
      msg:"删除失败",
    }
    ```
