```sh
# Clone项目
git clone https://gitee.com/huguangju/archive-api.git
cd archive-api

# 安装依赖
npm install

# 开发
npm run dev

# 部署
npm start
```

[API 文档](API.md)
