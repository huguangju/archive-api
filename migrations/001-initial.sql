--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------

CREATE TABLE Archive (
  id  INTEGER PRIMARY KEY,
  type TEXT NOT NULL,
  title TEXT NOT NULL,
  fullName TEXT,
  year TEXT,
  keepDate TEXT,
  boxNo TEXT,
  unitNo TEXT,
  liablePerson TEXT,
  time TEXT,
  pageCount TEXT,
  secretLevel TEXT,
  remark TEXT,
  fileType TEXT,
  folderName TEXT
);

-- INSERT INTO Archive (id, type, title, fullName, year, keepDate, boxNo, unitNo, liablePerson, time, pageCount, secretLevel, remark, fileType, folderName) VALUES (1, 'Qing', '题名96', '档案全名', '1999', '2017-12-22', '123123', '1223123', '李四', '2017-10-10', '339', '12', '备注', '清朝档案', '0d5eb32082-eee86-6a885-b653c-e3799edcb93be5a');
-- INSERT INTO Archive (id, type, title, fullName, year, keepDate, boxNo, unitNo, liablePerson, time, pageCount, secretLevel, remark, fileType, folderName) VALUES (2, 'Qing', '题名962', '档案全名2', '1999', '2017-12-22', '123123', '1223123', '李四', '2017-10-10', '339', '12', '备注', '清朝档案', '0d5eb32082-eee86-6a885-b653c-e3799edcb93be5a');
-- INSERT INTO Archive (id, type, title, fullName, year, keepDate, boxNo, unitNo, liablePerson, time, pageCount, secretLevel, remark, fileType, folderName) VALUES (3, 'Qing', '题名963', '档案全名3', '1999', '2017-12-22', '123123', '1223123', '李四', '2017-10-10', '339', '12', '备注', '清朝档案', '0d5eb32082-eee86-6a885-b653c-e3799edcb93be5a');

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------

DROP TABLE Archive;
