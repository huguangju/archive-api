import app from './app';
import Promise from 'bluebird';
import db from 'sqlite';

const { PORT = 10000 } = process.env;

Promise.resolve()
  // First, try connect to the database
  .then(() => db.open('./database.sqlite', { Promise }))
  .then(() => db.migrate({ force: 'last' }))
  .catch(err => console.error(err.stack))
  // Finally, launch Node.js app
  .finally(() => app.listen(PORT, () => console.log(`Listening on port ${PORT}`))); // eslint-disable-line no-console
